﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdfConverter.DataContracts.DataModels
{
    public class EmailMessage
    {
        public string Subject { get; set; }
        public StringBuilder Content { get; set; }
        public List<String> Recipients { get; set; }
    }
}

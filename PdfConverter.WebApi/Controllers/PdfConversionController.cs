using Microsoft.AspNetCore.Mvc;
using PdfConverter.Repository.Interfaces;
using System.Net;
using System.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;

namespace PdfConverterApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PdfConversionController : ControllerBase
    {
        private readonly IPdfConversionRepository _pdfConversionRepository;
        private readonly ILogger<PdfConversionController> _logger;
        public PdfConversionController(IPdfConversionRepository pdfConversionRepository, ILogger<PdfConversionController> logger)
        {
            _pdfConversionRepository = pdfConversionRepository;
            _logger = logger;
        }

        /**
        * Tests a connection to the server
        */
        [HttpGet("TestConnection")]
        public IActionResult TestConnection()
        {
            return Ok("Connected to PDF Conversion API");
        }

        /**
        * Sends an simple test message to appropriate stakeholders by email. Simply for testing email functionality.
        * Environment is defined through manual publish process specified inside of pubxml file. All emails sent on production 
        * will be sent to the entire team: Jeff, Kurt, Foster, Acedevelopers. Any emails sent on development build will be sent
        * to only Foster and Acedevelopers. This list can be changed in MailHelper.cs
        */
        [HttpGet("TestEmail")]
        public async Task<IActionResult> TestEmail()
        {
            try
            {
                _logger.LogInformation("Starting Test Email...");
                bool sent = await _pdfConversionRepository.TestEmail();
                if (sent)
                {
                    _logger.LogInformation("Conversion Completed Successfully!");
                    return Ok("Sent email with no errors.");
                }
                else
                {
                    return StatusCode(500, "Email Failed... Check Logs folder for more details.");
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return StatusCode(500, e.Message + "Check Logs folder for more details");
            }
        }

        /**
         * Uses DataLogics (in App_Data) to compress a string of html and return a pdf.
         * If compression fails, an email is sent to the list of emails based on environment defined in MailHelper.cs
         */
        [HttpPost("ConvertHtml")]
        public async Task<IActionResult> ConvertHtml([FromBody] string html)
        {
            try
            {
                _logger.LogInformation("Starting Conversion...");
                byte[] pdfBytes = await _pdfConversionRepository.ConvertHtmlToPdf(html);
                _logger.LogInformation("Conversion Completed Successfully!");
                _logger.LogInformation("nBytes returned: " + pdfBytes.Length);
                return new FileStreamResult(new MemoryStream(pdfBytes), "application/pdf");
            }
            catch (Exception e)
            {
                _logger.LogError(e.ToString());
                return StatusCode(500, e.Message);
            }
        }
    }
}
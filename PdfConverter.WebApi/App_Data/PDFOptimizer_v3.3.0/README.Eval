--------------------------------------------------------------------------------
Datalogics, Inc.			              (312) 853-8200
Chicago, IL  					http://www.datalogics.com
March, 2023
--------------------------------------------------------------------------------

HOW TO RUN AN EVALUATION COPY OF THIS RELEASE

If you are evaluating PDFOPTIMIZER prior to purchasing it, you will have
received an Evaluation release. This is a time-limited version which
requires a license.  A license can be generated automatically using the
evaluation key, a 16-digit number that is provided in the email
that accompanies the download link.

 - Generating an evaluation license

A time limited node locked license can be automatically generated on any machine
with internet access.  If you would like to evaluate PDFOPTIMIZER on a machine
that does not have internet connectivity, please contact the Datalogics sales team.
At the end of the setup procedure, PDFOPTIMIZER will start up, and when prompted
for an activation key, enter the key that was provided in the download email.
Input the key formatted as it is in the email, XXXX-YYYY-ZZZZ-NNNN, sixteen
digits with three dashes.

A license file named "pdfoptimizer.lic" will be created in the installation directory,
with Administrator priviliges.  The Datalogics sales team can make changes to
your license, normally to extend your evaluation period, and when a change
is made PDFOPTIMIZER (not the setup program) will need to be run again with
Administrator priviliges.

 - Using Proxy Servers

PDFOPTIMIZER will attempt to auto-detect the proxy server on Windows systems,
and if that fails, read on to set the proxy manually.

The activation client software will make use of a user-defined proxy server.
To use a proxy server, set the environment variable HTTP_PROXY or http_proxy to
the hostname and port number of the proxy server.  For example, to utilize the
HTTP proxy server running on "myproxyhost" on port 8765, use the following
command on unix:

% set HTTP_PROXY=myproxyhost:8765

If your proxy server uses authentication, you use the HTTP_PROXY_CREDENTIALS
environment variable to pass the credentials to the proxy server:
HTTP_PROXY_CREDENTIALS - the username and password to authenticate you to the
proxy server, in the format user:password.
For example, if your username is "joe" and password is "joes_password":

% set HTTP_PROXY_CREDENTIALS=joe:joes_password

Note that activation supports only the BASIC authentication type.

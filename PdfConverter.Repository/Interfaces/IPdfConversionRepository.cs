﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdfConverter.Repository.Interfaces
{
    public interface IPdfConversionRepository
    {
        Task<byte[]> ConvertHtmlToPdf(string html);
        Task<bool> TestEmail();
    }
}

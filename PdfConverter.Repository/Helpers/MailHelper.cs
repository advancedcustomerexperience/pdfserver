﻿using Microsoft.Extensions.Logging;
using PdfConverter.Repository.Enums;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace PdfConverter.Repository.Helpers
{
    public class MailHelper
    {
        private readonly ILogger<MailHelper> _logger;
        private readonly ISendGridClient _sendGridClient;
        private readonly List<EmailAddress> _developerEmailList;
        private readonly List<EmailAddress> _teamEmailList;

        public MailHelper(ILogger<MailHelper> logger, ISendGridClient sendGridClient)
        {
            _logger = logger;
            _sendGridClient = sendGridClient;
            _developerEmailList = new List<EmailAddress>(){
                new EmailAddress(
                    "foster.morgan@andersencorp.com",
                    "Foster Morgan"
                ),
                new EmailAddress(
                    "acedevelopers@andersencorp.com",
                    "Andersen Developers"
                )
            };
            _teamEmailList = new List<EmailAddress>()
            {
                new EmailAddress(
                    "foster.morgan@andersencorp.com",
                    "Foster Morgan"
                ),
                new EmailAddress(
                    "acedevelopers@andersencorp.com",
                    "Andersen Developers"
                ),
                new EmailAddress(
                    "jeffrey.lien@andersencorp.com",
                    "Jeffrey Lien"
                ),
                new EmailAddress(
                    "kurt.neuburger@andersencorp.com",
                    "Kurt Neuburger"
                )
            };
        }

        public async Task<bool> SendErrorEmailsAsync(string emailMessage, string subject, EmailToListEnum emailToList)
        {
            _logger.LogInformation("In SendErrorEmailsAsync...");

            //FROM
            var msg = new SendGridMessage()
            {
                From = new EmailAddress("pdfconverter.service@andersencorp.com", "PdfConverter Service"),
                Subject = subject
            };

            // CONTENT
            msg.AddContent(MimeType.Html, emailMessage);

            // TO
            if (emailToList == EmailToListEnum.DEVELOPERS)
            {
                msg.AddTos(_developerEmailList);
            }
            else if (emailToList == EmailToListEnum.TEAM)
            {
                msg.AddTos(_teamEmailList);
            }

            // SEND
            Response? response = await _sendGridClient.SendEmailAsync(msg).ConfigureAwait(false);
            _logger.LogInformation("Response Status Code: " + response.StatusCode);

            return response.IsSuccessStatusCode;
        }
    }
}

﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using PdfConverter.Repository.Enums;
using PdfConverter.Repository.Helpers;
using PdfConverter.Repository.Interfaces;
using System.Diagnostics;
using System.Text;

namespace PdfConverter.Repository.Implementations
{
    public class PdfConversionRepository : IPdfConversionRepository
    {
        private readonly ILogger<PdfConversionRepository> _logger;
        private readonly MailHelper _mailHelper;
        private readonly IHostEnvironment _host;
        public PdfConversionRepository(ILogger<PdfConversionRepository> logger, MailHelper mailHelper, IHostEnvironment host)
        {
            _logger = logger;
            _mailHelper = mailHelper;
            _host = host;
        }

        public async Task<bool> TestEmail()
        {
            string subject = "Testing PDF compressor functionality";
            string testMessage = "THIS IS A TEST EMAIL. If you are seeing this, the test was successful and you can ignore this email. :)";

            bool isProd = _host.IsProduction();

            bool result = await _mailHelper.SendErrorEmailsAsync(testMessage, subject, isProd ? EmailToListEnum.TEAM : EmailToListEnum.DEVELOPERS);

            return result;
        }

        public async Task<byte[]> ConvertHtmlToPdf(string html)
        {
            // Create temp file for html and write to it
            var root = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data");

            // Convert HTML to PDF
            var convertedPdf = RunConverterCommand(root, html);

            // Compress PDF
            var compressedPdf = await RunCompressionCommand(root, convertedPdf);

            // Read bytes of compressedPdf 
            byte[] bytes = File.ReadAllBytes(compressedPdf);

            // Delete compressed file in App Data
            if (File.Exists(compressedPdf))
            {
                File.Delete(compressedPdf);
            }

            return bytes;
        }

        private string RunConverterCommand(string root, string html)
        {
            var htmlFile = GetTempFile(root, "html");

            File.WriteAllText(htmlFile, html);

            // Create temp file for pdf
            var pdfFile = GetTempFile(root, "pdf");

            // Convert HTML to PDF using Chrome Headless in a process
            string convertCmd = string.Format("Chrome\\chrome --headless --print-to-pdf=\"{0}\" \"{1}\"", pdfFile, htmlFile);

            SetUpAndStartProcess(convertCmd, root);

            // Delete
            if (File.Exists(htmlFile))
            {
                File.Delete(htmlFile);
            }

            return pdfFile;
        }

        private async Task<string> RunCompressionCommand(string root, string originalPdf)
        {
            // Compress pdf
            var compressedPdf = GetTempFile(root, "pdf");

            string optimizerRootFolder = "PDFOptimizer_v3.3.0";

            var profile = Path.Combine(root, optimizerRootFolder, "OptimizationProfiles", "compressionMedium.json");

            string compressCmd = string.Format("\"{0}\\" + optimizerRootFolder + "\\pdfoptimizer.exe\" --input \"{1}\" --output \"{2}\" --profile \"{3}\"", root, originalPdf, compressedPdf, profile);
            _logger.LogInformation("CompressCmd: " + compressCmd);

            string cmdOutput = SetUpAndStartProcess(compressCmd, root);

            // Delete file if it exists and successfully created a new compressedPdf
            if (File.Exists(originalPdf) && File.Exists(compressedPdf))
            {
                File.Delete(originalPdf);
                return compressedPdf;
            }
            //otherwise compressed file wasn't created...
            _logger.LogCritical("Failure to compress pdf, returned the original version. See above for more details.");

            // Notify the ACETeam that compression failed
            DateTime today = DateTime.Now;
            string body = "An error has occured when compressing a PDF. The original, non-compressed pdf was returned to the user and stored in Azure." +
                "<br />PdfOptimizer Ouput Details:<br/>" + cmdOutput + "<br /><br />" + 
                "A log file can be found on the AzureVM: AZNCPWPDF01 as 'C:\\apps\\HtmlToPdfApi\\logs\\pdfcompresser-" + today.Year + today.Month.ToString("00") + today.Day.ToString("00") + ".log' for more details.";

            string subject = "FATAL: TechPubs PDF Compression Failure";
            bool isProd = _host.IsProduction();

            await _mailHelper.SendErrorEmailsAsync(body.ToString(), subject, isProd ? EmailToListEnum.TEAM : EmailToListEnum.DEVELOPERS);

            //return original, non-compressed pdf
            return originalPdf;
        }

        private string GetTempFile(string directory, string extension)
        {
            //generate guid to use as unique temp filename
            var tempFileName = Guid.NewGuid().ToString();

            tempFileName = Path.ChangeExtension(tempFileName, extension);

            var file = Path.Combine(directory, tempFileName);
            return file;
        }

        private string SetUpAndStartProcess(string command, string directory)
        {
            StringBuilder output = new StringBuilder();
            try
            {
                var si = new ProcessStartInfo();
                si.RedirectStandardInput = true;
                si.RedirectStandardOutput = true;
                si.RedirectStandardError = true;
                si.FileName = "cmd.exe";
                si.WindowStyle = ProcessWindowStyle.Normal;
                si.UseShellExecute = false; // use false to hide window
                si.WorkingDirectory = directory;

                var process = new Process();
                process.StartInfo = si;

                using (var outputWaitHandle = new AutoResetEvent(false))
                {
                    using (var errorWaitHandle = new AutoResetEvent(false))
                    {
                        process.OutputDataReceived += (sender, e) =>
                        {
                            if (e.Data != null)
                            {
                                _logger.LogInformation("Ouput: " + e.Data.ToString());
                                output.AppendLine("<br />Ouput: " + e.Data.ToString());
                            }
                            outputWaitHandle.Set();
                        };

                        process.ErrorDataReceived += (sender, e) =>
                        {
                            if (e.Data != null)
                            {
                                _logger.LogInformation("Error: " + e.Data.ToString());
                                output.AppendLine("<br />Error: " + e.Data.ToString());
                            }
                            errorWaitHandle.Set();
                        };

                        process.Start();

                        process.StandardInput.WriteLine(command);
                        process.StandardInput.Flush();
                        process.StandardInput.Close();


                        process.BeginOutputReadLine();
                        process.BeginErrorReadLine();

                        process.WaitForExit();

                        process.Close();
                    }
                }
                
            }
            catch (Exception e)
            {
                _logger.LogError("Error: " + e.Data.ToString());
                output.AppendLine("Exception Caught: " + e.Message);
            }
            return output.ToString();
        }
    }
}
